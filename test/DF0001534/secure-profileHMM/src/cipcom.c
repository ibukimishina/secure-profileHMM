#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <gmp.h>
#include <mpfr.h>
#include <paillier.h>

#include "../include/cipcom.h"
#include "../config/mydef.h"

/*************************

    cipher text function

**************************/

void init_rand( gmp_randstate_t rand, paillier_get_rand_t get_rand, int bytes );

void enc(CIPHERTEXT ca, PLAINTEXT a, paillier_pubkey_t* pub, paillier_get_rand_t get_rand)
{
    mpz_t r;
    gmp_randstate_t rand;
    mpz_t x;

    /* pick random blinding factor */
    mpz_init(r);
    init_rand(rand, get_rand, pub->bits / 8 + 1);
    do
        mpz_urandomb(r, rand, pub->bits);
    while( mpz_cmp(r, pub->n) >= 0 );

    /* compute ciphertext */
    mpz_init(x);
    mpz_powm(ca, pub->n_plusone, a, pub->n_squared);
    mpz_powm(x, r, pub->n, pub->n_squared);

    mpz_mul(ca, ca, x);
    mpz_mod(ca, ca, pub->n_squared);

    mpz_clear(x);
    mpz_clear(r);
    gmp_randclear(rand);
}

void dec(PLAINTEXT a, CIPHERTEXT ca, paillier_pubkey_t* pub, paillier_prvkey_t* prv)
{
    mpz_powm(a, ca, prv->lambda, pub->n_squared);
    mpz_sub_ui(a, a, 1);
    mpz_div(a, a, pub->n);
    mpz_mul(a, a, prv->x);
    mpz_mod(a, a, pub->n);
}

void add(paillier_pubkey_t* pub, CIPHERTEXT res, CIPHERTEXT ct0, CIPHERTEXT ct1)
{
    mpz_mul(res, ct0, ct1);
    mpz_mod(res, res, pub->n_squared);
}

void mul(paillier_pubkey_t* pub, CIPHERTEXT res, CIPHERTEXT ct, PLAINTEXT pt)
{
    mpz_powm(res, ct, pt, pub->n_squared);
}



/***************************

    communicate function

****************************/

void sendPub(int sockfd, paillier_pubkey_t* pub)
{
    int send_result;

    for(int i=0; i<(KEY_BIT/64); i++)
    {
        send_result = send(sockfd,&(*((pub->n->_mp_d)+i)),sizeof(*((pub->n->_mp_d)+i)),0);
        if(send_result == -1)
        {
            printf("send error\n");
            exit(1);
        }
    }
}

void recvPub(int sockfd, paillier_pubkey_t* pub)
{
    pub->bits = KEY_BIT;
    mpz_init(pub->n);
    mpz_init(pub->n_plusone);
    mpz_init(pub->n_squared);

    int recv_result;
    unsigned long a[KEY_BIT/64];

    mpfr_t bits;
    mpfr_t b[KEY_BIT/64];
    mpfr_t c[KEY_BIT/64];
    mpfr_t d[KEY_BIT/64];
    mpfr_t result;

    mpfr_init2(bits, CIP_BIT);
    mpfr_set_d(bits, 64.0, MPFR_RNDD);
    mpfr_init2(result, CIP_BIT);
    mpfr_set_d(result, 0.0, MPFR_RNDD);

    for(int i=0; i<(KEY_BIT/64); i++)
    {
        mpfr_init2(b[i], CIP_BIT);
        mpfr_init2(c[i], CIP_BIT);
        mpfr_init2(d[i], CIP_BIT);

        mpfr_mul_ui(b[i], bits, i, MPFR_RNDD);
        mpfr_exp2(c[i], b[i], MPFR_RNDD);

        recv_result = recv(sockfd,&(a[i]),sizeof(a[i]),0);
        if(recv_result == -1)
        {
            printf("pub recv error\n");
            exit(1);
        }

        mpfr_mul_ui(d[i], c[i], a[i], MPFR_RNDU);
        mpfr_add(result, result, d[i], MPFR_RNDD);
    }

    mpz_t zresult;
    mpz_init(zresult);
    mpfr_get_z(zresult, result, MPFR_RNDD);
    mpz_set(pub->n, zresult);
    
    mpz_add_ui(pub->n_plusone, pub->n, 1);
    mpz_mul(pub->n_squared, pub->n, pub->n);
}

void sendCip(int sockfd, CIPHERTEXT ca)
{
    int send_result;

    for(int i=0; i<(CIP_BIT/64); i++)
    {
        send_result = send(sockfd,&(*((ca->_mp_d)+i)),sizeof(*((ca->_mp_d)+i)),0);
        if(send_result == -1)
        {
            printf("send error\n");
            exit(1);
        }
    }
}

void recvCip(int sockfd, CIPHERTEXT ca)
{
    int recv_result;
    unsigned long a[CIP_BIT/64];

    mpfr_t bits;
    mpfr_t b[CIP_BIT/64];
    mpfr_t c[CIP_BIT/64];
    mpfr_t d[CIP_BIT/64];
    mpfr_t result;

    mpfr_init2(bits, CIP_BIT);
    mpfr_set_d(bits, 64.0, MPFR_RNDD);
    mpfr_init2(result, CIP_BIT);
    mpfr_set_d(result, 0.0, MPFR_RNDD);

    for(int i=0; i<(CIP_BIT/64); i++)
    {
        mpfr_init2(b[i], CIP_BIT);
        mpfr_init2(c[i], CIP_BIT);
        mpfr_init2(d[i], CIP_BIT);

        mpfr_mul_ui(b[i], bits, i, MPFR_RNDD);
        mpfr_exp2(c[i], b[i], MPFR_RNDD);

        recv_result = recv(sockfd,&(a[i]),sizeof(a[i]),0);
        if(recv_result == -1)
        {
            printf("recv error\n");
            exit(1);
        }

        mpfr_mul_ui(d[i], c[i], a[i], MPFR_RNDU);
        mpfr_add(result, result, d[i], MPFR_RNDD);
    }

    mpz_t zresult;
    mpz_init(zresult);
    mpfr_get_z(zresult, result, MPFR_RNDD);
    mpz_set(ca, zresult);
}
