my $input = $ARGV[0];

my @array = ();
my @line = ();
my @line_data = ();

mkdir "param";

my $fh;
open($fh, "<", $input) or die "$!";
@array = readline $fh;
close($fh);

my @array_transition;
my @array_emission_i;
my @array_emission_m;

my $j = 0;
my $k = 0;
my $l = 0;
for(my $i=0; $i<=$#array; $i++)
{
	my $mod = ($i + 1) % 3;

	if($mod == 1)
	{
		$array_emission_m[$j] = $array[$i];
		$j++;
	} 

	elsif($mod == 2)
	{
		$array_emission_i[$k] = $array[$i];
		$k++;
	}

	elsif($mod == 0)
	{
		$array_transition[$l] = $array[$i];
		$l++;
	}
}

# trim array_emission_m
for(my $i=0; $i<=$#array_emission_m; $i++)
{
	chomp($array_emission_m[$i]);

	$array_emission_m[$i] =~ s/^\s+//;
	$array_emission_m[$i] =~ s/COMPO/0/;

	if($i < 10)
	{
		$array_emission_m[$i] =~ s/^.//;
	}

	elsif($i <100)
	{
		$array_emission_m[$i] =~ s/^..//;
	}

	elsif($i < 1000)
	{
		$array_emission_m[$i] =~ s/^...//;
	}

	elsif($i < 10000)
	{
		$array_emission_m[$i] =~ s/^....//;
	}

	else
	{
		$array_emission_m[$i] =~ s/^.....//;
	}
	
	$array_emission_m[$i] =~ s/^\s+//;

	my @line = split(/  /, $array_emission_m[$i], 5);

	for(my $j=0; $j<4; $j++)
	{
		$array_emission_m_trim[$i][$j] = $line[$j];
	}
}

open($fh, ">", "param/emission_m.txt") or die "output error!:$!";
for(my $i=0; $i<=$#array_emission_m_trim; $i++)
{
	for(my $j=0; $j<4; $j++)
	{
		print $fh "$array_emission_m_trim[$i][$j] ";
	}
	print $fh "\n";
}
close($fh);

# trim array_emission_i
my @array_emission_i_trim = ();
for(my $i=0; $i<=$#array_emission_i; $i++)
{
	chomp($array_emission_i[$i]);

	$array_emission_i[$i] =~ s/^\s+//;

	my @line = split(/  /, $array_emission_i[$i]);

	for(my $j=0; $j<4; $j++)
	{
		$array_emission_i_trim[$i][$j] = $line[$j];
	}
}

open($fh, ">", "param/emission_i.txt") or die "output error!:$!";
for(my $i=0; $i<=$#array_emission_i_trim; $i++)
{
	for(my $j=0; $j<4; $j++)
	{
		print $fh "$array_emission_i_trim[$i][$j] ";
	}
	print $fh "\n";
}
close($fh);

# trim_transition
my @array_transition_trim = ();
for(my $i=0; $i<=$#array_transition; $i++)
{
	chomp($array_transition[$i]);

	$array_transition[$i] =~ s/^\s+//;
	$array_transition[$i] =~ s/(?:......\*)/0.00000/g;

	my @line = split(/  /, $array_transition[$i]);

	for(my $j=0; $j<7; $j++)
	{
		$array_transition_trim[$i][$j] = $line[$j];
	}
}

my @transition_m_m = ();
my @transition_m_i = ();
my @transition_m_d = ();
my @transition_i_m = ();
my @transition_i_i = ();
my @transition_d_m = ();
my @transition_d_d = ();

for(my $i=0; $i<=$#array_transition_trim; $i++)
{
	$transition_m_m[$i] = $array_transition_trim[$i][0];
	$transition_m_i[$i] = $array_transition_trim[$i][1];
	$transition_m_d[$i] = $array_transition_trim[$i][2];
	$transition_i_m[$i] = $array_transition_trim[$i][3];
	$transition_i_i[$i] = $array_transition_trim[$i][4];
	$transition_d_m[$i] = $array_transition_trim[$i][5];
	$transition_d_d[$i] = $array_transition_trim[$i][6];
}

my $filename1 = "transition_m_m.txt";
&fileoutput($filename1, \@transition_m_m);

my $filename2 = "transition_m_i.txt";
&fileoutput($filename2, \@transition_m_i);

my $filename3 = "transition_m_d.txt";
&fileoutput($filename3, \@transition_m_d);

my $filename4 = "transition_i_m.txt";
&fileoutput($filename4, \@transition_i_m);

my $filename5 = "transition_i_i.txt";
&fileoutput($filename5, \@transition_i_i);

my $filename6 = "transition_d_m.txt";
&fileoutput($filename6, \@transition_d_m);

my $filename7 = "transition_d_d.txt";
&fileoutput($filename7, \@transition_d_d);

sub fileoutput
{
	my $myFH;
	my ($filename, $ref) = @_;
	my @array = @$ref;

	open($myFH, ">", "param/$filename") or die "output error!:$!";

	for(my $i=0; $i<=$#array; $i++)
	{
		print $myFH "$array[$i]\n";
		
	}
	close($myFH);
}