#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <paillier.h>

#include "../include/phmmfunc_client.h"
#include "../include/cipcom.h"
#include "../config/mydef.h"



/******************************

	secure exponent protocol  

*******************************/

void AliceCalcBetatheta
(CIPHERTEXT cbetatheta, 
	paillier_prvkey_t* prv, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT clogbetatheta, 
	mpfr_t D)
{
	mpfr_t gamma;
	mpfr_init_set_ui(gamma, GAMMA, MPFR_RNDD);

	PLAINTEXT plogbetatheta;
	plain_init(plogbetatheta);
	dec(plogbetatheta, clogbetatheta, pub, prv);

	mpfr_t logbetathetaD;
	mpfr_init2(logbetathetaD, ACCURACY);
	mpfr_set_z(logbetathetaD, plogbetatheta, MPFR_RNDD);

	mpfr_t logbetatheta;
	mpfr_init2(logbetatheta, ACCURACY);
	mpfr_div(logbetatheta, logbetathetaD, D, MPFR_RNDD);

	mpfr_t neglogbetatheta;
	mpfr_init2(neglogbetatheta, ACCURACY);
	mpfr_neg(neglogbetatheta, logbetatheta, MPFR_RNDD);

	mpfr_t betathetaG;
	mpfr_init2(betathetaG, ACCURACY);
	mpfr_exp(betathetaG, neglogbetatheta, MPFR_RNDD);

	mpfr_t betatheta;
	mpfr_init2(betatheta, ACCURACY);
	mpfr_div(betatheta, betathetaG, gamma, MPFR_RNDD);

	mpfr_t betathetaD;
	mpfr_init2(betathetaD, ACCURACY);
	mpfr_mul(betathetaD, betatheta, D, MPFR_RNDD);

	mpz_t gmpbetatheta;
	mpz_init2(gmpbetatheta, ACCURACY);
	mpfr_get_z(gmpbetatheta, betathetaD, MPFR_RNDD);

	enc(cbetatheta, gmpbetatheta, pub, paillier_get_rand_devurandom);

}


/********************************

	secure logarithm protocol   

*********************************/

void AliceCalcLogbetatheta
(CIPHERTEXT clogbetatheta, 
	paillier_prvkey_t* prv, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT cbetatheta, 
	mpfr_t D)
{
	mpfr_t num;
	mpfr_init_set_ui(num, INTEGER, MPFR_RNDU);

	PLAINTEXT pbetatheta;
	plain_init(pbetatheta);
	dec(pbetatheta, cbetatheta, pub, prv);

	mpfr_t betathetaDnum;
	mpfr_init2(betathetaDnum, ACCURACY);
	mpfr_set_z(betathetaDnum, pbetatheta, MPFR_RNDU);
	
	int a = mpfr_zero_p(betathetaDnum);

	mpz_t gmplogbetatheta;

	if(a != 0)
	{
		mpz_init_set_str(gmplogbetatheta, "1000", 10);
		mpfr_t b;
		mpfr_init2(b, ACCURACY);
		mpfr_mul_z(b, D, gmplogbetatheta, MPFR_RNDU);
		mpfr_get_z(gmplogbetatheta, b, MPFR_RNDU);

		// mpz_init_set_str(gmplogbetatheta, "100000000000000000000000000000000000000000000000000000", 10);

	} else {

		mpfr_t betathetaD;
		mpfr_init2(betathetaD, ACCURACY);
		mpfr_div(betathetaD, betathetaDnum, num, MPFR_RNDD);

		mpfr_t betatheta;
		mpfr_init2(betatheta, ACCURACY);
		mpfr_div(betatheta, betathetaD, D, MPFR_RNDD);

		mpfr_t logbetatheta;
		mpfr_init2(logbetatheta, ACCURACY);
		mpfr_log(logbetatheta, betatheta, MPFR_RNDD);

		mpfr_t logbetathetaD;
		mpfr_init2(logbetathetaD, ACCURACY);
		mpfr_mul(logbetathetaD, logbetatheta, D, MPFR_RNDD);

		mpfr_t neglogbetathetaD;
		mpfr_init2(neglogbetathetaD, ACCURACY);
		mpfr_neg(neglogbetathetaD, logbetathetaD, MPFR_RNDD);

		// encrypts log(betatheta)
		mpz_init2(gmplogbetatheta, ACCURACY);
		mpfr_get_z(gmplogbetatheta, neglogbetathetaD, MPFR_RNDD);

	}
	
	enc(clogbetatheta, gmplogbetatheta, pub, paillier_get_rand_devurandom);

}
