/*************************
	important setting
*************************/

//bit number of paillier encrypto system
#define BIT 256

// number of match state
#define MATCH 72

// Length of input sequence 
#define LENGTH 72

// accuracy of plain text(for initialize)
#define ACCURACY 512

// bit of random number
#define R 32





/* DO NOT EDIT */

// The number of digits(plain text)
#define NUM 50

// The number to Change transition probability to integer
#define INTEGER 10000000000

// gamma for secure exponent protocol
#define GAMMA 73513440

#define KEY_BIT BIT
#define CIP_BIT KEY_BIT*2


/*************************
	parameter setting 
**************************/

#define TES 0.00 // I->D or D->I


/***********
	macro
***********/

#define p(a) mpfr_out_str(stdout, 10, 0, a, MPFR_RNDU)
#define ln() printf("\n");
#define pp(a) gmp_printf("%Zd\n", a)

#define cip_init(a) mpz_init_set_ui(a, 1)
#define plain_init(a) mpz_init(a)

#define CIPHERTEXT mpz_t
#define PLAINTEXT mpz_t

// Number of trials(default T = 1)
#define T 1