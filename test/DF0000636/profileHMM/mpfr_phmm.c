#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/resource.h>

#include <gmp.h>
#include <mpfr.h>

#include "mydef.h"

int AliceConvertSeqtoNum(char base)
{
	int baseNum;
	if(base == 'A')
		{
			baseNum = 0;
		} else if(base == 'C') {
			baseNum = 1;
		} else if(base == 'G') {
			baseNum = 2;
		} else if(base == 'T') {
			baseNum = 3;
		} else {
			printf("error\n");
			exit(0);
		}
	return baseNum;
}

int main(int argc, char **argv)
{
	double time_total;

	int try = T;
	for(int t = 0; t < try; t++)
	{
		clock_t start, end;
		start = clock();


	// parameter setting
	int L = LENGTH;
	int M = MATCH;

	// mpfr_t sig;
	// mpfr_t eps;
	mpfr_t tes;

	// mpfr_init_set_d(sig, SIG, MPFR_RNDU);
	// mpfr_init_set_d(eps, EPS, MPFR_RNDU);
	mpfr_init_set_d(tes, TES, MPFR_RNDU);

	/******************** 
		input sequence 
	*********************/
	FILE *fp;

	char *input_file;
	input_file = argv[1];

	char filepath[256];
	sprintf(filepath, "%s", argv[1]);

	char sequence[L];
	
	fp = fopen(filepath, "r");

	int scan=0;
	int c;
	while ((c = fgetc(fp)) != EOF )
    {
        sequence[scan] = c;
        scan++;
    }
    fclose(fp);

    printf("input sequence = ");
    for(int i=0; i<L; i++)
    {
    	printf("%c",sequence[i]);
    }
    printf("\n");

    int input[L];
    // printf("input\n");
    for(int i=0; i<L; i++)
    {
    	input[i] = AliceConvertSeqtoNum(sequence[i]);
    	// printf("%d", input[i]);
    }
    // printf("\n");


	/**************************** 
		Transition probability
	*****************************/
	double m_m_double[M+1];
	double m_i_double[M+1];
	double m_d_double[M+1];
	double i_m_double[M+1];
	double i_i_double[M+1];
	double d_m_double[M+1];
	double d_d_double[M+1];

	fp = fopen("param/transition_m_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_i_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&i_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_d_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&d_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_m_i.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_i_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_m_d.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_d_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_i_i.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&i_i_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_d_d.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&d_d_double[i]); }
	fclose(fp);

    mpfr_t a_m_m[M+1];
    mpfr_t a_m_i[M+1];
    mpfr_t a_m_d[M+1];
	mpfr_t a_i_m[M+1];
	mpfr_t a_i_i[M+1];
	mpfr_t a_d_m[M+1];
	mpfr_t a_d_d[M+1];

	for(int i=0; i<M+1; i++)
	{
		mpfr_init_set_d(a_m_m[i], m_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_m_i[i], m_i_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_m_d[i], m_d_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_i_m[i], i_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_i_i[i], i_i_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_d_m[i], d_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_d_d[i], d_d_double[i], MPFR_RNDU);
	}

	for(int i=0; i<M+1; i++)
	{
		mpfr_neg(a_m_m[i], a_m_m[i], MPFR_RNDU);
		mpfr_neg(a_m_i[i], a_m_i[i], MPFR_RNDU);
		mpfr_neg(a_m_d[i], a_m_d[i], MPFR_RNDU);
		mpfr_neg(a_i_m[i], a_i_m[i], MPFR_RNDU);
		mpfr_neg(a_i_i[i], a_i_i[i], MPFR_RNDU);
		mpfr_neg(a_d_m[i], a_d_m[i], MPFR_RNDU);
		mpfr_neg(a_d_d[i], a_d_d[i], MPFR_RNDU);

		mpfr_exp(a_m_m[i], a_m_m[i], MPFR_RNDU);
		mpfr_exp(a_m_i[i], a_m_i[i], MPFR_RNDU);
		mpfr_exp(a_m_d[i], a_m_d[i], MPFR_RNDU);
		mpfr_exp(a_i_m[i], a_i_m[i], MPFR_RNDU);
		mpfr_exp(a_i_i[i], a_i_i[i], MPFR_RNDU);
		mpfr_exp(a_d_m[i], a_d_m[i], MPFR_RNDU);
		mpfr_exp(a_d_d[i], a_d_d[i], MPFR_RNDU);
	}

	mpfr_set_d(a_d_m[0], 0.0, MPFR_RNDU);

	mpfr_set_d(a_d_d[0], 0.0, MPFR_RNDU);
	mpfr_set_d(a_d_d[M], 0.0, MPFR_RNDU);

	mpfr_set_d(a_m_d[M], 0.0, MPFR_RNDU);	



	/************************* 
		output probability
	**************************/

	/***********
		Match 
	************/
	// printf("emission match\n");

	double e_m_double[M+1][4];
	fp = fopen("param/emission_m.txt", "r");

	for(int i=0;i<M+1;i++)
	{
		for(int j=0;j<4;j++)
		{
			fscanf(fp,"%lf",&e_m_double[i][j]);
		}
	}

	for(int i=0; i<4; i++)
	{
		e_m_double[0][i] = 0.0;
	}
	fclose(fp);

	mpfr_t e_m[M+1][4];
	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			mpfr_init_set_d(e_m[i][j], e_m_double[i][j], MPFR_RNDU);

			// p(e_m[i][j]);
			// printf(" ");
		}

		// printf("\n");
	}

	/***********
		Insert 
	************/
	// printf("emission insert\n");

	double e_i_double[M+1][4];
	fp = fopen("param/emission_i.txt", "r");

	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			fscanf(fp,"%lf",&e_i_double[i][j]);
		}
	}
	fclose(fp);

	mpfr_t e_i[M+1][4];
	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			mpfr_init_set_d(e_i[i][j], e_i_double[i][j], MPFR_RNDU);

			// p(e_i[i][j]);
			// printf(" ");
		}

		// printf("\n");
	}


	mpfr_t loge_m[M+1][4];
	mpfr_t loge_i[M+1][4];

	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			mpfr_init(loge_m[i][j]);
			mpfr_init(loge_i[i][j]);

			mpfr_neg(loge_m[i][j], e_m[i][j], MPFR_RNDU);
			mpfr_neg(loge_i[i][j], e_i[i][j], MPFR_RNDU);
		}
	}

	/*************************

		Dynamic programing

	**************************/

	mpfr_t F[M+1][L+1][3]; // 0=match 1=insertion 2=deletion

	// initialize
	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<L+1; j++)
		{
			for(int k=0; k<3; k++)
			{
				mpfr_init_set_d(F[i][j][k], 0.0, MPFR_RNDD);
			}
		}
	}

	// F[1][0][2] = log(sig * exp(F[0][0][0]));
	mpfr_exp(F[1][0][2], F[0][0][0], MPFR_RNDU);
	mpfr_mul(F[1][0][2], F[1][0][2], a_m_d[0], MPFR_RNDU);
	mpfr_log(F[1][0][2], F[1][0][2], MPFR_RNDU);


	// F[0][1][1] = log(e_i[0][input[0]]) + log(sig * exp(F[0][0][0]));
	mpfr_exp(F[0][1][1], F[0][0][0], MPFR_RNDU);
	mpfr_mul(F[0][1][1], F[0][1][1], a_m_i[0], MPFR_RNDU);
	mpfr_log(F[0][1][1], F[0][1][1], MPFR_RNDU);
	mpfr_add(F[0][1][1], F[0][1][1], loge_i[0][input[0]], MPFR_RNDU);

	for(int i=2; i<M+1; i++)
	{
		// F[i][0][2] = log(eps * exp(F[i-1][0][2]));
		mpfr_exp(F[i][0][2], F[i-1][0][2], MPFR_RNDU);
		mpfr_mul(F[i][0][2], F[i][0][2], a_d_d[i-1], MPFR_RNDU);
		mpfr_log(F[i][0][2], F[i][0][2], MPFR_RNDU);
	}

	for(int j=2; j<L+1; j++)
	{
		// F[0][j][1] = log(e_i[0][input[j-1]]) + log(eps * exp(F[0][j-1][1]));
		mpfr_exp(F[0][j][1], F[0][j-1][1], MPFR_RNDU);
		mpfr_mul(F[0][j][1], F[0][j][1], a_i_i[0], MPFR_RNDU);
		mpfr_log(F[0][j][1], F[0][j][1], MPFR_RNDU);
		mpfr_add(F[0][j][1], F[0][j][1], loge_i[0][input[j-1]], MPFR_RNDU);
	}

	// F[1][1][0] = log(e_m[1][input[0]]) + log(a_m_m[0] * exp(F[0][0][0]));
	mpfr_exp(F[1][1][0], F[0][0][0], MPFR_RNDU);
	mpfr_mul(F[1][1][0], F[1][1][0], a_m_m[0], MPFR_RNDU);
	mpfr_log(F[1][1][0], F[1][1][0], MPFR_RNDU);
	mpfr_add(F[1][1][0], F[1][1][0], loge_m[1][input[0]], MPFR_RNDU);

	// F[1][1][1] = log(e_i[1][input[0]]) + log(tes * exp(F[1][0][2]));
	mpfr_exp(F[1][1][1], F[1][0][2], MPFR_RNDU);
	mpfr_mul(F[1][1][1], F[1][1][1], tes, MPFR_RNDU);
	mpfr_set_ui(F[1][1][1], 1000000, MPFR_RNDU);
	mpfr_neg(F[1][1][1], F[1][1][1], MPFR_RNDU);
	// mpfr_log(F[1][1][1], F[1][1][1], MPFR_RNDU);
	mpfr_add(F[1][1][1], F[1][1][1], loge_i[1][input[0]], MPFR_RNDU);

	// F[1][1][2] = log(tes * exp(F[0][1][1]));
	mpfr_exp(F[1][1][2], F[0][1][1], MPFR_RNDU);
	mpfr_mul(F[1][1][2], F[1][1][2], tes, MPFR_RNDU);
	mpfr_set_ui(F[1][1][2], 1000000, MPFR_RNDU);
	mpfr_neg(F[1][1][2], F[1][1][2], MPFR_RNDU);
	// mpfr_log(F[1][1][2], F[1][1][2], MPFR_RNDU);

	mpfr_t tmp, tmp2;
	mpfr_init(tmp);
	mpfr_init(tmp2);

	for(int i=2; i<M+1; i++)
	{
		for(int k=0; k<3; k++)
		{
			if(k==0)
			{
				// F[i][1][k] = log(e_m[i][input[0]]) + log(a_d_m[i-1] * exp(F[i-1][0][2]));
				mpfr_exp(F[i][1][k], F[i-1][0][2], MPFR_RNDU);
				mpfr_mul(F[i][1][k], F[i][1][k], a_d_m[i-1], MPFR_RNDU);
				mpfr_log(F[i][1][k], F[i][1][k], MPFR_RNDU);
				mpfr_add(F[i][1][k], F[i][1][k], loge_m[i][input[0]], MPFR_RNDU);
			} 

			else if(k==1) 
			{
				// F[i][1][k] = log(e_i[i][input[0]]) + log(tes * exp(F[i][0][2]));
				mpfr_exp(F[i][1][k], F[i][0][2], MPFR_RNDU);
				mpfr_mul(F[i][1][k], F[i][1][k], tes, MPFR_RNDU);
				mpfr_set_ui(F[i][1][k], 1000000, MPFR_RNDU);
				mpfr_neg(F[i][1][k], F[i][1][k], MPFR_RNDU);
				// mpfr_log(F[i][1][k], F[i][1][k], MPFR_RNDU);
				mpfr_add(F[i][1][k], F[i][1][k], loge_i[i][input[0]], MPFR_RNDU);
			} 

			else if(k==2)
			{
				// F[i][1][k] = log(sig*exp(F[i-1][1][0]) + tes*exp(F[i-1][1][1]) + eps*exp(F[i-1][1][2]));
				mpfr_exp(tmp, F[i-1][1][0], MPFR_RNDU);
				mpfr_mul(tmp, tmp, a_m_d[i-1], MPFR_RNDU);

				mpfr_exp(tmp2, F[i-1][1][1], MPFR_RNDU);
				mpfr_mul(tmp2, tmp2, tes, MPFR_RNDU);

				mpfr_exp(F[i][1][k], F[i-1][1][2], MPFR_RNDU);
				mpfr_mul(F[i][1][k], F[i][1][k], a_d_d[i-1], MPFR_RNDU);

				mpfr_add(F[i][1][k], F[i][1][k], tmp, MPFR_RNDU);
				mpfr_add(F[i][1][k], F[i][1][k], tmp2, MPFR_RNDU);

				mpfr_log(F[i][1][k], F[i][1][k], MPFR_RNDU);
			}		
		}
	}

	for(int j=2; j<L+1; j++)
	{
		for(int k=0; k<3; k++)
		{
			if(k==0)
			{
				// F[1][j][k] = log(e_m[1][input[j-1]]) + log(a_i_m[0] * exp(F[0][j-1][1]));
				mpfr_exp(F[1][j][k], F[0][j-1][1], MPFR_RNDU);
				mpfr_mul(F[1][j][k], F[1][j][k], a_i_m[0], MPFR_RNDU);
				mpfr_log(F[1][j][k], F[1][j][k], MPFR_RNDU);
				mpfr_add(F[1][j][k], F[1][j][k], loge_m[1][input[j-1]], MPFR_RNDU);
			} 

			else if(k==1) 
			{
				// F[1][j][k] = log(e_i[1][input[j-1]]) + log(sig*exp(F[1][j-1][0]) + eps*exp(F[1][j-1][1]) + tes*exp(F[1][j-1][2]));
				mpfr_exp(tmp, F[1][j-1][0], MPFR_RNDU);
				mpfr_mul(tmp, tmp, a_m_i[1], MPFR_RNDU);

				mpfr_exp(tmp2, F[1][j-1][1], MPFR_RNDU);
				mpfr_mul(tmp2, tmp2, a_i_i[1], MPFR_RNDU);

				mpfr_exp(F[1][j][k], F[1][j-1][2], MPFR_RNDU);
				mpfr_mul(F[1][j][k], F[1][j][k], tes, MPFR_RNDU);

				mpfr_add(F[1][j][k], F[1][j][k], tmp, MPFR_RNDU);
				mpfr_add(F[1][j][k], F[1][j][k], tmp2, MPFR_RNDU);

				mpfr_log(F[1][j][k], F[1][j][k], MPFR_RNDU);
				mpfr_add(F[1][j][k], F[1][j][k], loge_i[1][input[j-1]], MPFR_RNDU);
			} 

			else if(k==2)
			{
				// F[1][j][k] = log(tes * exp(F[0][j][1]));
				mpfr_exp(F[1][j][k], F[0][j][1], MPFR_RNDU);
				mpfr_mul(F[1][j][k], F[1][j][k], tes, MPFR_RNDU);
				mpfr_set_ui(F[1][j][k], 1000000, MPFR_RNDU);
				mpfr_neg(F[1][j][k], F[1][j][k], MPFR_RNDU);
				// mpfr_log(F[1][j][k], F[1][j][k], MPFR_RNDU);
			}
		}
	}



	// Recursive expression
	for(int i=2; i<M+1; i++)
	{

		for(int j=2; j<L+1; j++)
		{
			for(int k=0; k<3; k++)
			{
				if(k==0)
				{
					// F[i][j][k] = log(e_m[i][input[j-1]]) + log(a_m_m[i-1]*exp(F[i-1][j-1][0]) + a_i_m[i-1]*exp(F[i-1][j-1][1]) + a_d_m[i-1]*exp(F[i-1][j-1][2]));
					mpfr_exp(tmp, F[i-1][j-1][0], MPFR_RNDU);
					mpfr_mul(tmp, tmp, a_m_m[i-1], MPFR_RNDU);

					mpfr_exp(tmp2, F[i-1][j-1][1], MPFR_RNDU);
					mpfr_mul(tmp2, tmp2, a_i_m[i-1], MPFR_RNDU);

					mpfr_exp(F[i][j][k], F[i-1][j-1][2], MPFR_RNDU);
					mpfr_mul(F[i][j][k], F[i][j][k], a_d_m[i-1], MPFR_RNDU);

					mpfr_add(F[i][j][k], F[i][j][k], tmp, MPFR_RNDU);
					mpfr_add(F[i][j][k], F[i][j][k], tmp2, MPFR_RNDU);

					mpfr_log(F[i][j][k], F[i][j][k], MPFR_RNDU);
					mpfr_add(F[i][j][k], F[i][j][k], loge_m[i][input[j-1]], MPFR_RNDU);
				} 

				else if(k==1) 
				{
					// F[i][j][k] = log(e_i[i][input[j-1]]) + log(sig*exp(F[i][j-1][0]) + eps*exp(F[i][j-1][1]) + tes*exp(F[i][j-1][2]));
					mpfr_exp(tmp, F[i][j-1][0], MPFR_RNDU);
					mpfr_mul(tmp, tmp, a_m_i[i], MPFR_RNDU);

					mpfr_exp(tmp2, F[i][j-1][1], MPFR_RNDU);
					mpfr_mul(tmp2, tmp2, a_i_i[i], MPFR_RNDU);

					mpfr_exp(F[i][j][k], F[i][j-1][2], MPFR_RNDU);
					mpfr_mul(F[i][j][k], F[i][j][k], tes, MPFR_RNDU);

					mpfr_add(F[i][j][k], F[i][j][k], tmp, MPFR_RNDU);
					mpfr_add(F[i][j][k], F[i][j][k], tmp2, MPFR_RNDU);

					mpfr_log(F[i][j][k], F[i][j][k], MPFR_RNDU);
					mpfr_add(F[i][j][k], F[i][j][k], loge_i[i][input[j-1]], MPFR_RNDU);
				} 

				else if(k==2)
				{
					// F[i][j][k] = log(sig*exp(F[i-1][j][0]) + tes*exp(F[i-1][j][1]) + eps*exp(F[i-1][j][2]));
					mpfr_exp(tmp, F[i-1][j][0], MPFR_RNDU);
					mpfr_mul(tmp, tmp, a_m_d[i-1], MPFR_RNDU);

					mpfr_exp(tmp2, F[i-1][j][1], MPFR_RNDU);
					mpfr_mul(tmp2, tmp2, tes, MPFR_RNDU);

					mpfr_exp(F[i][j][k], F[i-1][j][2], MPFR_RNDU);
					mpfr_mul(F[i][j][k], F[i][j][k], a_d_d[i-1], MPFR_RNDU);

					mpfr_add(F[i][j][k], F[i][j][k], tmp, MPFR_RNDU);
					mpfr_add(F[i][j][k], F[i][j][k], tmp2, MPFR_RNDU);

					mpfr_log(F[i][j][k], F[i][j][k], MPFR_RNDU);
				}
			}
		}

	}


	// check
	// for(int j=0; j<L+1; j++)
	// {
	// 	for(int k=0; k<3; k++)
	// 	{
	// 		for(int i=0; i<M+1; i++)
	// 		{
	// 			// printf("F[%d][%d][%d]:%f ", i,j,k, F[i][j][k]);
	// 			printf("F[%d][%d][%d]:", i,j,k);
	// 			p(F[i][j][k]);
	// 			printf(" ");
	// 		}
	// 		printf("\n");
	// 	}
	// 	printf("\n");
	// }

	// double result = a_m_m[L]*exp(F[M][L][0]) + a_i_m[L]*exp(F[M][L][1]) + a_d_m[L]*exp(F[M][L][2]);
	mpfr_t result;
	mpfr_init(result);

	mpfr_exp(tmp, F[M][L][0], MPFR_RNDU);
	// p(tmp);
	// ln();
	mpfr_mul(tmp, tmp, a_m_m[M], MPFR_RNDU);

	mpfr_exp(tmp2, F[M][L][1], MPFR_RNDU);
	// p(tmp2);
	// ln();
	mpfr_mul(tmp2, tmp2, a_i_m[M], MPFR_RNDU);

	mpfr_exp(result, F[M][L][2], MPFR_RNDU);
	// p(result);
	// ln();
	mpfr_mul(result, result, a_d_m[M], MPFR_RNDU);

	mpfr_add(result, result, tmp, MPFR_RNDU);
	mpfr_add(result, result, tmp2, MPFR_RNDU);

	printf("result = ");
	p(result);
	ln();

	mpfr_t log_result;
	mpfr_init(log_result);
	mpfr_log(log_result, result, MPFR_RNDU);
	printf("log result = ");
	p(log_result);
	ln();

	end = clock();

	// time check
	double time = (double)(end - start) * 1000 / CLOCKS_PER_SEC;
	time_total += time;

	}

	double time_ave = time_total / try;
	printf("time = %f [msec]\n", time_ave);

	// getrusage関数による最大消費メモリ量のチェック
	int chk;
	struct rusage usage;
	chk = getrusage(RUSAGE_SELF, &usage);
	if(chk != 0){
	printf("error\n");
	exit(-1);
	}
	// このプロセスが消費した最大メモリ領域
	printf("Max RSS = %lf MB\n", usage.ru_maxrss / 1024.0);
}