/************************************

	generate divisor of 73513440

************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>

#include "../include/gen_rand.h"
#include "../config/mydef.h"

void mpfr_rand(mpfr_t rand)
{
	mpz_t r;
	mpz_init(r);

	gmp_randstate_t state;
	gmp_randinit_default(state);
	gmp_randseed_ui(state, (unsigned long)time(NULL));
	mpz_urandomb(r, state, R);

	mpfr_set_z(rand, r,  MPFR_RNDU);
}

int genRandom(int A, int N)
{
	srand(time(NULL));

	mpfr_t a;
	mpfr_init_set_ui(a, A, MPFR_RNDU);

	int int_rop = rand() % N - 1;

	mpfr_t res;
	mpfr_init(res);

	if(int_rop < 0)
	{
		mpfr_init_set_ui(res, 1, MPFR_RNDU);

	} else {

		mpfr_init_set(res, a, MPFR_RNDU);

		for(int i=0; i<int_rop; i++)
		{
			mpfr_mul(res, res, a, MPFR_RNDU);
		}
	}

	int result = mpfr_get_ui(res, MPFR_RNDU);

	return result;
} 

int genDivisor()
{
	int res4 = genRandom(7, 1);
	int res1 = genRandom(2, 6);
	int res5 = genRandom(11, 2);
	int res6 = genRandom(13, 2);
	int res2 = genRandom(3, 4);
	int res3 = genRandom(5, 2);
	int res7 = genRandom(17, 2);

	int result = res1 * res2 * res3 * res4 * res5 * res6 * res7;

	return result;
}
