#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <paillier.h>

#include "../include/phmmfunc_server.h"
#include "../include/gen_rand.h"
#include "../include/cipcom.h"
#include "../config/mydef.h"


/******************************

	secure exponent protocol  

*******************************/

void BobCalcLogbetatheta
(CIPHERTEXT clogbetatheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT clogtheta, 
	mpfr_t beta, 
	mpfr_t D)
{
	mpfr_t logbeta;
	mpfr_init2(logbeta, ACCURACY);
	mpfr_log(logbeta, beta, MPFR_RNDD);

	mpfr_t logbetaD;
	mpfr_init2(logbetaD, ACCURACY);
	mpfr_mul(logbetaD, logbeta, D, MPFR_RNDD);

	mpz_t gmplogbeta;
	mpz_init2(gmplogbeta, ACCURACY);
	mpfr_get_z(gmplogbeta, logbetaD, MPFR_RNDD);

	CIPHERTEXT clogbeta;
	cip_init(clogbeta);
	enc(clogbeta, gmplogbeta, pub, paillier_get_rand_devurandom);

	add(pub, clogbetatheta, clogtheta, clogbeta);

}

void BobCalcTheta
(CIPHERTEXT ctheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT cbetatheta, 
	mpfr_t beta)
{
	mpfr_t arpha, gamma;
	mpfr_init2(arpha, ACCURACY);
	mpfr_init_set_ui(gamma, GAMMA, MPFR_RNDD);
	mpfr_mul(arpha, gamma, beta, MPFR_RNDD);

	mpz_t gmparpha;
	mpz_init2(gmparpha, ACCURACY);
	mpfr_get_z(gmparpha, arpha, MPFR_RNDD);

	mul(pub, ctheta, cbetatheta, gmparpha);

}

/********************************

	secure logarithm protocol   

*********************************/

void BobCalcBetatheta
(CIPHERTEXT cbetatheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT ctheta, 
	mpfr_t beta)
{
	mpz_t gmpbeta;
	mpz_init(gmpbeta);
	mpfr_get_z(gmpbeta, beta, MPFR_RNDD);
	
	mul(pub, cbetatheta, ctheta, gmpbeta);
}

void BobCalcLogtheta
(mpz_t clogtheta, 
	paillier_pubkey_t* pub, 
	mpz_t clogbetatheta, 
	mpfr_t beta, mpfr_t D)
{
	mpfr_t logbeta;
	mpfr_init2(logbeta, ACCURACY);
	mpfr_log(logbeta, beta, MPFR_RNDD);

	mpfr_t logbetaD;
	mpfr_init2(logbetaD, ACCURACY);
	mpfr_mul(logbetaD, logbeta, D, MPFR_RNDD);

	mpz_t gmplogbeta;
	mpz_init2(gmplogbeta, ACCURACY);
	mpfr_get_z(gmplogbeta, logbetaD, MPFR_RNDD);

	mpz_t clogbeta;
	cip_init(clogbeta);
	enc(clogbeta, gmplogbeta, pub, paillier_get_rand_devurandom);

	add(pub, clogtheta, clogbetatheta, clogbeta);

}