#ifndef _PHMMFUNC_CLIENT_H_
#define _PHMMFUNC_CLIENT_H_

#define CIPHERTEXT mpz_t
#define PLAINTEXT mpz_t


// secure exponent protocol
// paillier_ciphertext_t* AliceCalcBetathetaCiphertext(paillier_ciphertext_t* cbetatheta, paillier_prvkey_t* prv, paillier_pubkey_t* pub, paillier_ciphertext_t* clogbetatheta, int D);
void AliceCalcBetatheta
(CIPHERTEXT cbetatheta, 
	paillier_prvkey_t* prv, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT clogbetatheta, 
	mpfr_t D);

// secure logarithm protocl
// paillier_ciphertext_t* AliceCalcLogbetathetaCiphertext(paillier_ciphertext_t* clogbetatheta, paillier_prvkey_t* prv, paillier_pubkey_t* pub, paillier_ciphertext_t* cbetatheta, int D);
void AliceCalcLogbetatheta
(CIPHERTEXT clogbetatheta, 
	paillier_prvkey_t* prv, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT cbetatheta, 
	mpfr_t D);

#endif // _PHMMFUNC_CLIENT_H_