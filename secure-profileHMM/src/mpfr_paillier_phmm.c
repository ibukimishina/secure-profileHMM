#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/resource.h>

#include <gmp.h>
#include <mpfr.h>
#include <paillier.h>

#include "../include/cipcom.h"
#include "../include/gen_rand.h"
#include "../include/phmmfunc_client.h"
#include "../include/phmmfunc_server.h"
#include "../config/mydef.h"

int main(int argc, char **argv)
{
	/**********************

		common setting    

	***********************/

	srand(time(NULL));

	// parameter setting
	int L = LENGTH;
	int M = MATCH;

	// random number
	int ra;
	mpfr_t r;
	mpfr_init2(r, R);

	mpfr_t num;
	mpfr_init_set_ui(num, NUM, MPFR_RNDU);
	
	mpfr_t D;
	mpfr_init2(D, ACCURACY);
	mpfr_exp10(D, num, MPFR_RNDU);
	
	/***********************************

			Alice initial setting      

	************************************/

	int try = T;
	double time_total;

	for(int t=0; t<try; t++)
	{

		clock_t start, end;
		start = clock();

	/***************************
		generate pub & prv key 
	****************************/
	paillier_pubkey_t* pub;
	paillier_prvkey_t* prv;
	paillier_keygen(BIT, &pub, &prv, paillier_get_rand_devurandom);

	/******************** 
		input sequence 
	*********************/
	FILE *fp;

	char *input;
	input = argv[1];

	char filepath[256];
	sprintf(filepath, "%s", argv[1]);

	char sequence[L];
	
	fp = fopen(filepath, "r");

	int scan=0;
	int c;
	while ((c = fgetc(fp)) != EOF )
    {
        sequence[scan] = c;
        scan++;
    }
    fclose(fp);

    printf("input sequence = ");
    for(int i=0; i<L; i++)
    {
    	printf("%c",sequence[i]);
    }
    printf("\n");


	/**************************************** 
		convert sequence -> 1 or 0 Matrix 
	*****************************************/
	int Matrix[L][4];

	for(int i = 0; i<LENGTH; i++)
	{
		if(sequence[i] == 'A')
		{
			Matrix[i][0] = 1;
			Matrix[i][1] = 0;
			Matrix[i][2] = 0;
			Matrix[i][3] = 0;
			

		} else if(sequence[i] == 'C') {
			Matrix[i][0] = 0;
			Matrix[i][1] = 1;
			Matrix[i][2] = 0;
			Matrix[i][3] = 0;
			

		} else if(sequence[i] == 'G') {
			Matrix[i][0] = 0;
			Matrix[i][1] = 0;
			Matrix[i][2] = 1;
			Matrix[i][3] = 0;
			
		} else if(sequence[i] == 'T') {
			Matrix[i][0] = 0;
			Matrix[i][1] = 0;
			Matrix[i][2] = 0;
			Matrix[i][3] = 1;
			

		} else {
			printf("input: encryption error\n");
			exit(0);
		}
	}

	/*****************
		encryption 
	******************/
	PLAINTEXT pMatrix[L][4];
	CIPHERTEXT cMatrix[L][4];

	for(int i=0; i<L; i++)
	{
		for(int j=0; j<4; j++)
		{
			plain_init(pMatrix[i][j]);
			cip_init(cMatrix[i][j]);

			mpz_set_ui(pMatrix[i][j], Matrix[i][j]);
			enc(cMatrix[i][j], pMatrix[i][j], pub, paillier_get_rand_devurandom);

		}
	}


	/***********************************

			Bob initial setting        

	************************************/

	/**************************** 
		Transition probability
	*****************************/
	
	mpfr_t tes;
	mpfr_init_set_d(tes, TES, MPFR_RNDU);
	int tes100 = TES * INTEGER;
	PLAINTEXT ptes100;
	mpz_init_set_ui(ptes100, tes100);

	double m_m_double[M+1];
	double m_i_double[M+1];
	double m_d_double[M+1];
	double i_m_double[M+1];
	double i_i_double[M+1];
	double d_m_double[M+1];
	double d_d_double[M+1];

	fp = fopen("param/transition_m_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_i_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&i_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_d_m.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&d_m_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_m_i.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_i_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_m_d.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&m_d_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_i_i.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&i_i_double[i]); }
	fclose(fp);

	fp = fopen("param/transition_d_d.txt", "r");
	for(int i=0; i<M+1; i++){ fscanf(fp,"%lf",&d_d_double[i]); }
	fclose(fp);

	mpfr_t a_m_m[M+1];
    mpfr_t a_m_i[M+1];
    mpfr_t a_m_d[M+1];
	mpfr_t a_i_m[M+1];
	mpfr_t a_i_i[M+1];
	mpfr_t a_d_m[M+1];
	mpfr_t a_d_d[M+1];

	for(int i=0; i<M+1; i++)
	{
		mpfr_init_set_d(a_m_m[i], m_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_m_i[i], m_i_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_m_d[i], m_d_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_i_m[i], i_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_i_i[i], i_i_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_d_m[i], d_m_double[i], MPFR_RNDU);
		mpfr_init_set_d(a_d_d[i], d_d_double[i], MPFR_RNDU);
	}

	for(int i=0; i<M+1; i++)
	{
		mpfr_neg(a_m_m[i], a_m_m[i], MPFR_RNDU);
		mpfr_neg(a_m_i[i], a_m_i[i], MPFR_RNDU);
		mpfr_neg(a_m_d[i], a_m_d[i], MPFR_RNDU);
		mpfr_neg(a_i_m[i], a_i_m[i], MPFR_RNDU);
		mpfr_neg(a_i_i[i], a_i_i[i], MPFR_RNDU);
		mpfr_neg(a_d_m[i], a_d_m[i], MPFR_RNDU);
		mpfr_neg(a_d_d[i], a_d_d[i], MPFR_RNDU);

		mpfr_exp(a_m_m[i], a_m_m[i], MPFR_RNDU);
		mpfr_exp(a_m_i[i], a_m_i[i], MPFR_RNDU);
		mpfr_exp(a_m_d[i], a_m_d[i], MPFR_RNDU);
		mpfr_exp(a_i_m[i], a_i_m[i], MPFR_RNDU);
		mpfr_exp(a_i_i[i], a_i_i[i], MPFR_RNDU);
		mpfr_exp(a_d_m[i], a_d_m[i], MPFR_RNDU);
		mpfr_exp(a_d_d[i], a_d_d[i], MPFR_RNDU);

		mpfr_mul_ui(a_m_m[i], a_m_m[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_m_i[i], a_m_i[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_m_d[i], a_m_d[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_i_m[i], a_i_m[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_i_i[i], a_i_i[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_d_m[i], a_d_m[i], INTEGER, MPFR_RNDU);
		mpfr_mul_ui(a_d_d[i], a_d_d[i], INTEGER, MPFR_RNDU);
	}

	mpfr_set_d(a_d_m[0], 0.0, MPFR_RNDU);

	mpfr_set_d(a_d_d[0], 0.0, MPFR_RNDU);
	mpfr_set_d(a_d_d[M], 0.0, MPFR_RNDU);

	mpfr_set_d(a_m_d[M], 0.0, MPFR_RNDU);	


	mpz_t pa_m_m[M+1];
    mpz_t pa_m_i[M+1];
    mpz_t pa_m_d[M+1];
	mpz_t pa_i_m[M+1];
	mpz_t pa_i_i[M+1];
	mpz_t pa_d_m[M+1];
	mpz_t pa_d_d[M+1];


	for(int i=0; i<M+1; i++)
	{
		plain_init(pa_m_m[i]);
		plain_init(pa_m_i[i]);
		plain_init(pa_m_d[i]);
		plain_init(pa_i_m[i]);
		plain_init(pa_i_i[i]);
		plain_init(pa_d_m[i]);
		plain_init(pa_d_d[i]);

		mpfr_get_z(pa_m_m[i], a_m_m[i], MPFR_RNDU);
		mpfr_get_z(pa_m_i[i], a_m_i[i], MPFR_RNDU);
		mpfr_get_z(pa_m_d[i], a_m_d[i], MPFR_RNDU);
		mpfr_get_z(pa_i_m[i], a_i_m[i], MPFR_RNDU);
		mpfr_get_z(pa_i_i[i], a_i_i[i], MPFR_RNDU);
		mpfr_get_z(pa_d_m[i], a_d_m[i], MPFR_RNDU);
		mpfr_get_z(pa_d_d[i], a_d_d[i], MPFR_RNDU);
	}



	/************************* 
		output probability
	**************************/

	/***********
		Match 
	************/
	double e_m_double[M+1][4];
	fp = fopen("param/emission_m.txt", "r");

	for(int i=0;i<M+1;i++)
	{
		for(int j=0;j<4;j++)
		{
			fscanf(fp,"%lf",&e_m_double[i][j]);
		}
	}

	for(int i=0; i<4; i++)
	{
		e_m_double[0][i] = 0.0;
	}

	fclose(fp);

	mpfr_t loge_m[M+1][4];
	PLAINTEXT ploge_m[M+1][4];

	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{

			plain_init(ploge_m[i][j]);
			
			mpfr_init_set_d(loge_m[i][j], e_m_double[i][j], MPFR_RNDU);
			mpfr_mul(loge_m[i][j], loge_m[i][j], D, MPFR_RNDU);

			mpfr_get_z(ploge_m[i][j], loge_m[i][j], MPFR_RNDU);
		}
	}



	/************* 
		Insert 
	**************/
	mpfr_t e_i[M+1][4];

	double e_i_double[M+1][4];
	fp = fopen("param/emission_i.txt", "r");

	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			fscanf(fp,"%lf",&e_i_double[i][j]);
		}
	}
	fclose(fp);



	mpfr_t loge_i[M+1][4];
	PLAINTEXT ploge_i[M+1][4];

	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<4; j++)
		{
			plain_init(ploge_i[i][j]);

			mpfr_init_set_d(loge_i[i][j], e_i_double[i][j], MPFR_RNDU);
			mpfr_mul(loge_i[i][j], loge_i[i][j], D, MPFR_RNDU);

			mpfr_get_z(ploge_i[i][j], loge_i[i][j], MPFR_RNDU);
		}
	}

	/*************************

		Bob receive Matrix   

	**************************/

	/***********
		Match 
	************/
	CIPHERTEXT coutput_m[L][M][4];
	
	PLAINTEXT test[L][M][4];

	for(int i=0; i<L; i++)
	{
		for(int j=0; j<M; j++)
		{
			for(int k=0; k<4; k++)
			{
				cip_init(coutput_m[i][j][k]);
				mul(pub, coutput_m[i][j][k], cMatrix[i][k], ploge_m[j+1][k]);
			}
		}
	}

	CIPHERTEXT clogOutput_m[L][M];
	PLAINTEXT plogOutput_m[L][M];

	for(int i=0; i<L; i++)
	{
		for(int j=0; j<M; j++)
		{
			cip_init(clogOutput_m[i][j]);

			for(int k=0; k<4; k++)
			{
				add(pub, clogOutput_m[i][j], clogOutput_m[i][j], coutput_m[i][j][k]);

				plain_init(plogOutput_m[i][j]);
			}
		}
	}


	/************* 
		Insert 
	**************/
	CIPHERTEXT coutput_i[L][M+1][4];

	for(int i=0; i<L; i++)
	{
		for(int j=0; j<M+1; j++)
		{
			for(int k=0; k<4; k++)
			{
				cip_init(coutput_i[i][j][k]);
				mul(pub, coutput_i[i][j][k], cMatrix[i][k], ploge_i[j][k]);
			}
		}
	}


	CIPHERTEXT clogOutput_i[L][M+1];

	for(int i=0; i<L; i++)
	{
		for(int j=0; j<M+1; j++)
		{
			cip_init(clogOutput_i[i][j]);

			for(int k=0; k<4; k++)
			{
				add(pub, clogOutput_i[i][j], clogOutput_i[i][j], coutput_i[i][j][k]);
			}
		}
	}



	/********************************

			Dinamic Programing      

	*********************************/


	// DP table
	// 0=match 1=insertion 2=deletion
	mpfr_t mpfr_F[M+1][L+1][3];
	mpz_t gmp_F[M+1][L+1][3];

	PLAINTEXT pF[M+1][L+1][3];
	CIPHERTEXT cF[M+1][L+1][3];

	// secure exponent protocol
	CIPHERTEXT cbetaF[M+1][L+1][3];
	CIPHERTEXT cbetaexpF[M+1][L+1][3];
	CIPHERTEXT cexpF[M+1][L+1][3];
	CIPHERTEXT caexpF[M+1][L+1][3];

	// secure logarithm protocol
	CIPHERTEXT csum[M+1][L+1][3];
	CIPHERTEXT cbetasum[M+1][L+1][3];
	CIPHERTEXT clogbetasum[M+1][L+1][3];
	CIPHERTEXT clogsum[M+1][L+1][3];


	for(int i=0; i<M+1; i++)
	{
		for(int j=0; j<L+1; j++)
		{
			for(int k=0; k<3; k++)
			{
				mpfr_init_set_d(mpfr_F[i][j][k], 0.0, MPFR_RNDU);
				mpz_init_set_ui(gmp_F[i][j][k], 0);

				plain_init(pF[i][j][k]);
				cip_init(cF[i][j][k]);

				cip_init(cbetaF[i][j][k]);
				cip_init(cbetaexpF[i][j][k]);
				cip_init(cexpF[i][j][k]);
				cip_init(caexpF[i][j][k]);

				cip_init(csum[i][j][k]);
				cip_init(cbetasum[i][j][k]);
				cip_init(clogbetasum[i][j][k]);
				cip_init(clogsum[i][j][k]);
			}
		}
	}

	

	/*****************
		initialize
	******************/

	// F[1][0][2] = log(sig * exp(F[0][0][0]));
	mpfr_exp(mpfr_F[1][0][2], mpfr_F[0][0][0], MPFR_RNDU);
	mpfr_mul(mpfr_F[1][0][2], mpfr_F[1][0][2], a_m_d[0], MPFR_RNDU);
	mpfr_log(mpfr_F[1][0][2], mpfr_F[1][0][2], MPFR_RNDU);

	// F[0][1][1] = log(e_i[0][input[0]]) + log(sig * exp(F[0][0][0]));
	mpfr_exp(mpfr_F[0][1][1], mpfr_F[0][0][0], MPFR_RNDU);
	mpfr_mul(mpfr_F[0][1][1], mpfr_F[0][1][1], a_m_i[0], MPFR_RNDU);
	mpfr_log(mpfr_F[0][1][1], mpfr_F[0][1][1], MPFR_RNDU);
	mpfr_mul(mpfr_F[0][1][1], mpfr_F[0][1][1], D, MPFR_RNDU);
	mpfr_abs(mpfr_F[0][1][1], mpfr_F[0][1][1], MPFR_RNDU);
	mpfr_get_z(gmp_F[0][1][1], mpfr_F[0][1][1], MPFR_RNDU);
	enc(cF[0][1][1], gmp_F[0][1][1], pub, paillier_get_rand_devurandom);
	add(pub, cF[0][1][1], cF[0][1][1], clogOutput_i[0][0]);	

	// F[i][0][2] = log(eps * exp(F[i-1][0][2]));
	for(int i=2; i<M+1; i++)
	{
		mpfr_exp(mpfr_F[i][0][2], mpfr_F[i-1][0][2], MPFR_RNDU);
		mpfr_mul(mpfr_F[i][0][2], mpfr_F[i][0][2], a_d_d[i-1], MPFR_RNDU);
		mpfr_log(mpfr_F[i][0][2], mpfr_F[i][0][2], MPFR_RNDU);
	}

	for(int i=1; i<M+1; i++)
	{
		mpfr_mul(mpfr_F[i][0][2], mpfr_F[i][0][2], D, MPFR_RNDU);
		mpfr_abs(mpfr_F[i][0][2], mpfr_F[i][0][2], MPFR_RNDU);
		mpfr_get_z(gmp_F[i][0][2], mpfr_F[i][0][2], MPFR_RNDU);

		enc(cF[i][0][2], gmp_F[i][0][2], pub, paillier_get_rand_devurandom);
	}



	// F[0][j][1] = log(e_i[0][input[j-1]]) + log(eps * exp(F[0][j-1][1]));
	for(int j=2; j<L+1; j++)
	{
		// secure exponent protocol
		ra = genDivisor(); 
		mpfr_set_ui(r, ra, MPFR_RNDU);
		BobCalcLogbetatheta(cbetaF[0][j-1][1], pub, cF[0][j-1][1], r, D);
		AliceCalcBetatheta(cbetaexpF[0][j-1][1], prv, pub, cbetaF[0][j-1][1], D);
		BobCalcTheta(cexpF[0][j-1][1], pub, cbetaexpF[0][j-1][1], r);

		mul(pub, caexpF[0][j-1][1], cexpF[0][j-1][1], pa_i_i[0]);

		// secure logarithm protocol
		mpz_set(csum[0][j][1], caexpF[0][j-1][1]);

		mpfr_rand(r); 
		// mpfr_set_ui(r, ra, MPFR_RNDU);
		BobCalcBetatheta(cbetasum[0][j][1], pub, csum[0][j][1], r);
		AliceCalcLogbetatheta(clogbetasum[0][j][1], prv, pub, cbetasum[0][j][1], D);
		BobCalcLogtheta(clogsum[0][j][1], pub, clogbetasum[0][j][1], r, D);

		add(pub, cF[0][j][1], clogsum[0][j][1], clogOutput_i[j-1][0]);
		
	}
	
	/**** calc F[1][1][0] ****/
	// secure exponent protocol
	ra = genDivisor(); 
	mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcLogbetatheta(cbetaF[0][0][0], pub, cF[0][0][0], r, D);
	AliceCalcBetatheta(cbetaexpF[0][0][0], prv, pub, cbetaF[0][0][0], D);
	BobCalcTheta(cexpF[0][0][0], pub, cbetaexpF[0][0][0], r);

	mul(pub, caexpF[0][0][0], cexpF[0][0][0], pa_m_m[0]);

	// secure logarithm protocol
	mpz_set(csum[1][1][0], caexpF[0][0][0]);

	mpfr_rand(r); 
	// mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcBetatheta(cbetasum[1][1][0], pub, csum[1][1][0], r);
	AliceCalcLogbetatheta(clogbetasum[1][1][0], prv, pub, cbetasum[1][1][0], D);
	BobCalcLogtheta(clogsum[1][1][0], pub, clogbetasum[1][1][0], r, D);

	add(pub, cF[1][1][0], clogsum[1][1][0], clogOutput_m[0][0]);

	/**** calc F[1][1][1] ****/
	// secure exponent protocol
	ra = genDivisor(); 
	mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcLogbetatheta(cbetaF[1][0][2], pub, cF[1][0][2], r, D);
	AliceCalcBetatheta(cbetaexpF[1][0][2], prv, pub, cbetaF[1][0][2], D);
	BobCalcTheta(cexpF[1][0][2], pub, cbetaexpF[1][0][2], r);

	mul(pub, caexpF[1][0][2], cexpF[1][0][2], ptes100);

	// secure logarithm protocol
	mpz_set(csum[1][1][1], caexpF[1][0][2]);

	mpfr_rand(r); 
	// mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcBetatheta(cbetasum[1][1][1], pub, csum[1][1][1], r);
	AliceCalcLogbetatheta(clogbetasum[1][1][1], prv, pub, cbetasum[1][1][1], D);
	BobCalcLogtheta(clogsum[1][1][1], pub, clogbetasum[1][1][1], r, D);

	add(pub, cF[1][1][1], clogsum[1][1][1], clogOutput_i[1][0]);

	/**** calc F[1][1][2] ****/
	// secure exponent protocol
	ra = genDivisor(); 
	mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcLogbetatheta(cbetaF[0][1][1], pub, cF[0][1][1], r, D);
	AliceCalcBetatheta(cbetaexpF[0][1][1], prv, pub, cbetaF[0][1][1], D);
	BobCalcTheta(cexpF[0][1][1], pub, cbetaexpF[0][1][1], r);

	mul(pub, caexpF[0][1][1], cexpF[0][1][1], ptes100);

	// secure logarithm protocol
	mpz_set(csum[1][1][2], caexpF[0][1][1]);

	mpfr_rand(r); 
	// mpfr_set_ui(r, ra, MPFR_RNDU);
	BobCalcBetatheta(cbetasum[1][1][2], pub, csum[1][1][2], r);
	AliceCalcLogbetatheta(clogbetasum[1][1][2], prv, pub, cbetasum[1][1][2], D);
	BobCalcLogtheta(clogsum[1][1][2], pub, clogbetasum[1][1][2], r, D);

	mpz_set(cF[1][1][2], clogsum[1][1][2]);


	/* calc F[i][1][k] */
	for(int i=2; i<M+1; i++)
	{
		for(int k=0; k<3; k++)
		{
			if(k==0)
			{
				// secure exponent protocol
				ra = genDivisor(); 
				mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcLogbetatheta(cbetaF[i-1][0][2], pub, cF[i-1][0][2], r, D);
				AliceCalcBetatheta(cbetaexpF[i-1][0][2], prv, pub, cbetaF[i-1][0][2], D);
				BobCalcTheta(cexpF[i-1][0][2], pub, cbetaexpF[i-1][0][2], r);

				mul(pub, caexpF[i-1][0][2], cexpF[i-1][0][2], pa_d_m[i-1]);

				// secure logarithm protocol
				mpz_set(csum[i][1][k], caexpF[i-1][0][2]);

				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[i][1][k], pub, csum[i][1][k], r);
				AliceCalcLogbetatheta(clogbetasum[i][1][k], prv, pub, cbetasum[i][1][k], D);
				BobCalcLogtheta(clogsum[i][1][k], pub, clogbetasum[i][1][k], r, D);

				add(pub, cF[i][1][k], clogsum[i][1][k], clogOutput_m[0][i-1]);
			} 

			else if(k==1) 
			{
				// secure exponent protocol
				ra = genDivisor(); 
				mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcLogbetatheta(cbetaF[i][0][2], pub, cF[i][0][2], r, D);
				AliceCalcBetatheta(cbetaexpF[i][0][2], prv, pub, cbetaF[i][0][2], D);
				BobCalcTheta(cexpF[i][0][2], pub, cbetaexpF[i][0][2], r);

				mul(pub, caexpF[i][0][2], cexpF[i][0][2], ptes100);

				// secure logarithm protocol
				mpz_set(csum[i][1][k], caexpF[i][0][2]);

				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[i][1][k], pub, csum[i][1][k], r);
				AliceCalcLogbetatheta(clogbetasum[i][1][k], prv, pub, cbetasum[i][1][k], D);
				BobCalcLogtheta(clogsum[i][1][k], pub, clogbetasum[i][1][k], r, D);

				add(pub, cF[i][1][k], clogsum[i][1][k], clogOutput_i[0][i]);
			} 

			else if(k==2)
			{
				// secure exponent protocol
				for(int l=0; l<3; l++)
				{
					ra = genDivisor(); 
					mpfr_set_ui(r, ra, MPFR_RNDU);
					BobCalcLogbetatheta(cbetaF[i-1][1][l], pub, cF[i-1][1][l], r, D);
					AliceCalcBetatheta(cbetaexpF[i-1][1][l], prv, pub, cbetaF[i-1][1][l], D);
					BobCalcTheta(cexpF[i-1][1][l], pub, cbetaexpF[i-1][1][l], r);

					if(l==0)
					{
						mul(pub, caexpF[i-1][1][l], cexpF[i-1][1][l], pa_m_d[i-1]);
					}

					else if(l==1)
					{
						mul(pub, caexpF[i-1][1][l], cexpF[i-1][1][l], ptes100);
					}

					else if(l==2)
					{
						mul(pub, caexpF[i-1][1][l], cexpF[i-1][1][l], pa_d_d[i-1]);
					}

					add(pub, csum[i][1][k], csum[i][1][k], caexpF[i-1][1][l]);
				
				}

				// secure logarithm protocol
				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[i][1][k], pub, csum[i][1][k], r);
				AliceCalcLogbetatheta(clogbetasum[i][1][k], prv, pub, cbetasum[i][1][k], D);
				BobCalcLogtheta(clogsum[i][1][k], pub, clogbetasum[i][1][k], r, D);

				mpz_set(cF[i][1][k], clogsum[i][1][k]);
			}		
		}
	}

	/* calc F[1][j][k] */
	for(int j=2; j<L+1; j++)
	{
		for(int k=0; k<3; k++)
		{
			if(k==0)
			{
				// secure exponent protocol
				ra = genDivisor(); 
				mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcLogbetatheta(cbetaF[0][j-1][1], pub, cF[0][j-1][1], r, D);
				AliceCalcBetatheta(cbetaexpF[0][j-1][1], prv, pub, cbetaF[0][j-1][1], D);
				BobCalcTheta(cexpF[0][j-1][1], pub, cbetaexpF[0][j-1][1], r);

				mul(pub, caexpF[0][j-1][1], cexpF[0][j-1][1], pa_i_m[0]);

				// secure logarithm protocol
				mpz_set(csum[1][j][k], caexpF[0][j-1][1]);

				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[1][j][k], pub, csum[1][j][k], r);
				AliceCalcLogbetatheta(clogbetasum[1][j][k], prv, pub, cbetasum[1][j][k], D);
				BobCalcLogtheta(clogsum[1][j][k], pub, clogbetasum[1][j][k], r, D);

				add(pub, cF[1][j][k], clogsum[1][j][k], clogOutput_m[j-1][0]);
			} 

			else if(k==1) 
			{
				// secure exponent protocol
				for(int l=0; l<3; l++)
				{
					ra = genDivisor(); 
					mpfr_set_ui(r, ra, MPFR_RNDU);
					BobCalcLogbetatheta(cbetaF[1][j-1][l], pub, cF[1][j-1][l], r, D);
					AliceCalcBetatheta(cbetaexpF[1][j-1][l], prv, pub, cbetaF[1][j-1][l], D);
					BobCalcTheta(cexpF[1][j-1][l], pub, cbetaexpF[1][j-1][l], r);

					if(l==0)
					{
						mul(pub, caexpF[1][j-1][l], cexpF[1][j-1][l], pa_m_i[1]);
					}

					else if(l==1)
					{
						mul(pub, caexpF[1][j-1][l], cexpF[1][j-1][l], pa_i_i[1]);
					}

					else if(l==2)
					{
						mul(pub, caexpF[1][j-1][l], cexpF[1][j-1][l], ptes100);
					}

					add(pub, csum[1][j][k], csum[1][j][k], caexpF[1][j-1][l]);
				
				}

				// secure logarithm protocol
				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[1][j][k], pub, csum[1][j][k], r);
				AliceCalcLogbetatheta(clogbetasum[1][j][k], prv, pub, cbetasum[1][j][k], D);
				BobCalcLogtheta(clogsum[1][j][k], pub, clogbetasum[1][j][k], r, D);

				add(pub, cF[1][j][k], clogsum[1][j][k], clogOutput_i[j-1][1]);
			} 

			else if(k==2)
			{
				// secure exponent protocol
				ra = genDivisor(); 
				mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcLogbetatheta(cbetaF[0][j][1], pub, cF[0][j][1], r, D);
				AliceCalcBetatheta(cbetaexpF[0][j][1], prv, pub, cbetaF[0][j][1], D);
				BobCalcTheta(cexpF[0][j][1], pub, cbetaexpF[0][j][1], r);

				mul(pub, caexpF[0][j][1], cexpF[0][j][1], ptes100);

				// secure logarithm protocol
				mpz_set(csum[1][j][k], caexpF[0][j][1]);

				mpfr_rand(r); 
				// mpfr_set_ui(r, ra, MPFR_RNDU);
				BobCalcBetatheta(cbetasum[1][j][k], pub, csum[1][j][k], r);
				AliceCalcLogbetatheta(clogbetasum[1][j][k], prv, pub, cbetasum[1][j][k], D);
				BobCalcLogtheta(clogsum[1][j][k], pub, clogbetasum[1][j][k], r, D);

				mpz_set(cF[1][j][k], clogsum[1][j][k]);
			}
		}
	}

	// Recursive expression
	for(int i=2; i<M+1; i++)
	{

		for(int j=2; j<L+1; j++)
		{
			for(int k=0; k<3; k++)
			{
				if(k==0)
				{
					// secure exponent protocol
					for(int l=0; l<3; l++)
					{
						ra = genDivisor(); 
						mpfr_set_ui(r, ra, MPFR_RNDU);
						BobCalcLogbetatheta(cbetaF[i-1][j-1][l], pub, cF[i-1][j-1][l], r, D);
						AliceCalcBetatheta(cbetaexpF[i-1][j-1][l], prv, pub, cbetaF[i-1][j-1][l], D);
						BobCalcTheta(cexpF[i-1][j-1][l], pub, cbetaexpF[i-1][j-1][l], r);

						if(l==0)
						{
							mul(pub, caexpF[i-1][j-1][l], cexpF[i-1][j-1][l], pa_m_m[i-1]);
						}

						else if(l==1)
						{
							mul(pub, caexpF[i-1][j-1][l], cexpF[i-1][j-1][l], pa_i_m[i-1]);
						}

						else if(l==2)
						{
							mul(pub, caexpF[i-1][j-1][l], cexpF[i-1][j-1][l], pa_d_m[i-1]);
						}

						add(pub, csum[i][j][k], csum[i][j][k], caexpF[i-1][j-1][l]);
					
					}

					// secure logarithm protocol
					mpfr_rand(r); 
					// mpfr_set_ui(r, ra, MPFR_RNDU);
					BobCalcBetatheta(cbetasum[i][j][k], pub, csum[i][j][k], r);
					AliceCalcLogbetatheta(clogbetasum[i][j][k], prv, pub, cbetasum[i][j][k], D);
					BobCalcLogtheta(clogsum[i][j][k], pub, clogbetasum[i][j][k], r, D);

					add(pub, cF[i][j][k], clogsum[i][j][k], clogOutput_m[j-1][i-1]);
				} 

				else if(k==1) 
				{
					// secure exponent protocol
					for(int l=0; l<3; l++)
					{
						ra = genDivisor(); 
						mpfr_set_ui(r, ra, MPFR_RNDU);
						BobCalcLogbetatheta(cbetaF[i][j-1][l], pub, cF[i][j-1][l], r, D);
						AliceCalcBetatheta(cbetaexpF[i][j-1][l], prv, pub, cbetaF[i][j-1][l], D);
						BobCalcTheta(cexpF[i][j-1][l], pub, cbetaexpF[i][j-1][l], r);

						if(l==0)
						{
							mul(pub, caexpF[i][j-1][l], cexpF[i][j-1][l], pa_m_i[i]);
						}

						else if(l==1)
						{
							mul(pub, caexpF[i][j-1][l], cexpF[i][j-1][l], pa_i_i[i]);
						}

						else if(l==2)
						{
							mul(pub, caexpF[i][j-1][l], cexpF[i][j-1][l], ptes100);
						}

						add(pub, csum[i][j][k], csum[i][j][k], caexpF[i][j-1][l]);
					
					}

					// secure logarithm protocol
					mpfr_rand(r); 
					// mpfr_set_ui(r, ra, MPFR_RNDU);
					BobCalcBetatheta(cbetasum[i][j][k], pub, csum[i][j][k], r);
					AliceCalcLogbetatheta(clogbetasum[i][j][k], prv, pub, cbetasum[i][j][k], D);
					BobCalcLogtheta(clogsum[i][j][k], pub, clogbetasum[i][j][k], r, D);

					add(pub, cF[i][j][k], clogsum[i][j][k], clogOutput_i[j-1][i]);
				} 

				else if(k==2)
				{
					// secure exponent protocol
					for(int l=0; l<3; l++)
					{
						ra = genDivisor(); 
						mpfr_set_ui(r, ra, MPFR_RNDU);
						BobCalcLogbetatheta(cbetaF[i-1][j][l], pub, cF[i-1][j][l], r, D);
						AliceCalcBetatheta(cbetaexpF[i-1][j][l], prv, pub, cbetaF[i-1][j][l], D);
						BobCalcTheta(cexpF[i-1][j][l], pub, cbetaexpF[i-1][j][l], r);

						if(l==0)
						{
							mul(pub, caexpF[i-1][j][l], cexpF[i-1][j][l], pa_m_d[i-1]);
						}

						else if(l==1)
						{
							mul(pub, caexpF[i-1][j][l], cexpF[i-1][j][l], ptes100);
						}

						else if(l==2)
						{
							mul(pub, caexpF[i-1][j][l], cexpF[i-1][j][l], pa_d_d[i-1]);
						}

						add(pub, csum[i][j][k], csum[i][j][k], caexpF[i-1][j][l]);
					
					}

					// secure logarithm protocol
					mpfr_rand(r); 
					// mpfr_set_ui(r, ra, MPFR_RNDU);
					BobCalcBetatheta(cbetasum[i][j][k], pub, csum[i][j][k], r);
					AliceCalcLogbetatheta(clogbetasum[i][j][k], prv, pub, cbetasum[i][j][k], D);
					BobCalcLogtheta(clogsum[i][j][k], pub, clogbetasum[i][j][k], r, D);

					mpz_set(cF[i][j][k], clogsum[i][j][k]);
				}
			}
		}
	}

	// secure exponent protocol
	mpz_t cresult;
	cip_init(cresult);

	for(int l=0; l<3; l++)
	{
		ra = genDivisor(); 
		mpfr_set_ui(r, ra, MPFR_RNDU);
		BobCalcLogbetatheta(cbetaF[M][L][l], pub, cF[M][L][l], r, D);
		AliceCalcBetatheta(cbetaexpF[M][L][l], prv, pub, cbetaF[M][L][l], D);
		BobCalcTheta(cexpF[M][L][l], pub, cbetaexpF[M][L][l], r);

		if(l==0)
		{
			mul(pub, caexpF[M][L][l], cexpF[M][L][l], pa_m_m[M]);
		}

		else if(l==1)
		{
			mul(pub, caexpF[M][L][l], cexpF[M][L][l], pa_i_m[M]);
		}

		else if(l==2)
		{
			mul(pub, caexpF[M][L][l], cexpF[M][L][l], pa_d_m[M]);
		}

		add(pub, cresult, cresult, caexpF[M][L][l]);
	
	}

	PLAINTEXT presult;
	plain_init(presult);
	dec(presult, cresult, pub, prv);

	
	mpfr_t res;
	mpfr_init(res);

	mpfr_set_z(res, presult, MPFR_RNDU);

	mpfr_div(res, res, D, MPFR_RNDU);
	mpfr_div_ui(res, res, INTEGER, MPFR_RNDU);


	end = clock();



	// time check
	double time = (double)(end - start) * 1000 / CLOCKS_PER_SEC;
	time_total += time;

	mpz_t result_matrix[M+1][L+1][3];
	mpfr_t ans[M+1][L+1][3];

	// for(int j=0; j<L+1; j++)
	// {
	// 	for(int k=0; k<3; k++)
	// 	{
	// 		for(int i=0; i<M+1; i++)
	// 		{
	// 			plain_init(result_matrix[i][j][k]);
	// 			// printf("F[%d][%d][%d]:%f ", i,j,k, F[i][j][k]);
	// 			// result_matrix[i][j][k] = dec(cF[i][j][k]);
	// 			dec(result_matrix[i][j][k], cF[i][j][k], pub, prv);

	// 			mpfr_init(ans[i][j][k]);
	// 			mpfr_set_z(ans[i][j][k], result_matrix[i][j][k], MPFR_RNDU);
	// 			mpfr_div(ans[i][j][k], ans[i][j][k], D, MPFR_RNDU);

	// 			printf("F[%d][%d][%d]:", i,j,k);
	// 			// gmp_printf("%Zd ", result_matrix[i][j][k]);
	// 			p(ans[i][j][k]);
	// 			printf(" ");
	// 		}
	// 		printf("\n");
	// 	}
	// 	printf("\n");
	// }


	printf("result = ");
	p(res);
	ln();

	mpfr_t log_result;
	mpfr_init(log_result);
	mpfr_log(log_result, res, MPFR_RNDU);
	printf("log result = ");
	p(log_result);
	ln();

	}


	// double time_ave = time_total / try;
	// printf("time = %f [msec]\n", time_ave);

	// getrusage関数による最大消費メモリ量のチェック
	int chk;
	struct rusage usage;
	chk = getrusage(RUSAGE_SELF, &usage);
	if(chk != 0){
	printf("error\n");
	exit(-1);
	}
	// このプロセスが消費した最大メモリ領域
	printf("Max RSS = %lf MB\n", usage.ru_maxrss / 1024.0);


}