#ifndef _PHMMFUNC_SERVER_H_
#define _PHMMFUNC_SERVER_H_

#define CIPHERTEXT mpz_t
#define PLAINTEXT mpz_t


// secure exponent protocol
// paillier_ciphertext_t* BobCalcLogbetathetaCiphertext(paillier_ciphertext_t* clogbetatheta, paillier_pubkey_t* pub, paillier_ciphertext_t* clogtheta, int beta, int D);
// paillier_ciphertext_t* BobCalcTheta(paillier_ciphertext_t* ctheta, paillier_pubkey_t* pub, paillier_ciphertext_t* cbetatheta, int beta);
void BobCalcLogbetatheta
(CIPHERTEXT clogbetatheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT clogtheta, 
	mpfr_t beta, 
	mpfr_t D);

void BobCalcTheta
(CIPHERTEXT ctheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT cbetatheta, 
	mpfr_t beta);

// secure logarithm protocol
// paillier_ciphertext_t* BobCalcBetathetaCiphertext(paillier_ciphertext_t* cbetatheta, paillier_pubkey_t* pub, paillier_ciphertext_t* ctheta, int beta);
// paillier_ciphertext_t* BobCalcLogthetaCiphertext(paillier_ciphertext_t* clogtheta, paillier_pubkey_t* pub, paillier_ciphertext_t* clogbetatheta, int beta, int D);
void BobCalcBetatheta
(CIPHERTEXT cbetatheta, 
	paillier_pubkey_t* pub, 
	CIPHERTEXT ctheta, 
	mpfr_t beta);

void BobCalcLogtheta
(mpz_t clogtheta, 
	paillier_pubkey_t* pub, 
	mpz_t clogbetatheta, 
	mpfr_t beta, mpfr_t D);
 
#endif // _PHMMFUNC_SERVER_H_