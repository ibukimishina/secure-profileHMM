#ifndef _CIPCOM_H_
#define _CIPCOM_H_

#define CIPHERTEXT mpz_t
#define PLAINTEXT mpz_t

/*************************

    cipher text function

**************************/

void enc(CIPHERTEXT ca, PLAINTEXT a, paillier_pubkey_t* pub, paillier_get_rand_t get_rand);
void dec(PLAINTEXT a, CIPHERTEXT ca, paillier_pubkey_t* pub, paillier_prvkey_t* prv);
void add(paillier_pubkey_t* pub, CIPHERTEXT res, CIPHERTEXT ct0, CIPHERTEXT ct1);
void mul(paillier_pubkey_t* pub, CIPHERTEXT res, CIPHERTEXT ct, PLAINTEXT pt);

/***************************

    communicate function

****************************/

void sendPub(int sockfd, paillier_pubkey_t* pub);
void recvPub(int sockfd, paillier_pubkey_t* pub);
void sendCip(int sockfd, CIPHERTEXT ca);
void recvCip(int sockfd, CIPHERTEXT ca);
 
#endif // _CIPCOM_H_