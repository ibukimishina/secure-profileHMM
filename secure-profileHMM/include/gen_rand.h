#ifndef _GEN_RAND_H_
#define _GEN_RAND_H_

void mpfr_rand(mpfr_t rand);
int genRandom(int A, int N);
int genDivisor();

#endif //_GEN_RAND_H_