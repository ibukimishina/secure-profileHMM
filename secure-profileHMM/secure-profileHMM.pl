use Time::HiRes;
use strict;
use warnings;
use Getopt::Long;

my $input_hmm = $ARGV[0];
my $input_sequence = $ARGV[1];

my $ignore = '';

GetOptions('ignore' => \$ignore);
if($ignore)
{
	print "not remove directory \"param\"\n";
	print "use already existing parameters\n";
}

else
{
	my $check_param = "param";
	if (-e $check_param) 
	{ 
		print "directory \"$check_param\" already exit\n";
		print "remove old directory \"param\"\n";
		rmdir "param";
	}

	else
	{
		print "directory \"$check_param\" not exit\n";
	}

	print "make new directory \"param\"\n";
	system("perl src/file_div.pl $input_hmm");
}

my $check_phmm = "bin/phmm";
if (-e $check_phmm) 
{ 
	print "$check_phmm already exit\n";
	print "remove old file \"bin/phmm\"\n";
	system("rm bin/phmm");
}

else
{
	print "file \"bin/phmm\" not exit\n";
}

print "make new file \"bin/phmm\"\n";
system("make");

my $start_time = Time::HiRes::time;

system("./bin/phmm $input_sequence");
   
printf("time = %f[sec]\n",Time::HiRes::time - $start_time);  